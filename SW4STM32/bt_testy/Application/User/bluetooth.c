/*
 * bluetooth.c
 *
 *  Created on: 12 mar 2020
 *      Author: Dawid
 */

#include "bluetooth.h"
#include <stdio.h>
#include <string.h>

UART_HandleTypeDef huart1;								// uart structure
volatile uint8_t data_rx_flag=0;						// flag from receiving data
bt_data bt_message={0,VELOCITY,{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},0}; //example default bt_message


volatile uint8_t data_identyfier=100;		//variable for storing received identyfier
uint8_t data_frame_rx[DATA_SIZE+20]="";		//buffer for rx data frame
//uint8_t data_frame_tx[DATA_SIZE+20]="";		//buffer for tx data frame
uint8_t uint_value[8]="";					//buffer for converted bytes value


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {	//example callback from receive 7bytes

	/*can be edited freely for specific purposes*/

	data_rx_flag=1;		//set the flag - data received

} //end of callback


static void data_recognition(bt_data *data, uint8_t received_identyfier)
{
	/*recognising and writing identyfier of data to data structure*/

	switch(received_identyfier)
	{
		case 0x30:		//0
		{
			BT_DATA_TYPE=VELOCITY;

		}

		break;

		case 0x31:	//'1' - 48 (in ASCII) - 48=0x31 (HEX)
		{
			BT_DATA_TYPE=ENGINE_TEMP;

		}

		break;

		case 0x32:		//2
		{
			BT_DATA_TYPE=LED_ON;

		}

		break;

		case 0x33:			//3
		{
			BT_DATA_TYPE=LED_OFF;

		}

		break;

		case 0x34:			//4
		{
			BT_DATA_TYPE=AIR_TEMP;

		}

		break;


		case 0x35:			//5
		{
			BT_DATA_TYPE=ACCELERATION;

		}

		break;

		case 0x36:			//6
		{
			BT_DATA_TYPE=BATTERY_LEVEL;

		}

		break;

		case 0x37:				//7
		{
			BT_DATA_TYPE=DISPLAY_DATA;

		}

		break;

		case 0x38:					//8
		{
			BT_DATA_TYPE=DRIVE_MODE;

		}

		break;

		case 0x39:					//9
		{
			BT_DATA_TYPE=DEBUG_ST;

		}

		break;
	}	//end of switch
}		//end of function


void bluetooth_send_data(bt_data *data)
{
	/*send data from data structure*/
	data->header=0x58;     						 //setting default header for our message
	(data->crc)=((data->header)+(data->txData[0])+(data->txData[1])+(data->txData[2])+(data->txData[3])+(data->txData[4])+(data->txData[5])+(data->txData[6])+(data->txData[7])+(data->type)) % 256; //computing CRC

	//sprintf((char*)data_frame_tx,"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",data->header,(uint8_t)(data->type),data->txData[0],data->txData[1],data->txData[2],data->txData[3],data->txData[4],data->txData[5],data->txData[6],data->txData[7],data->crc);

	uint8_t data_frame_tx[DATA_SIZE]={data->header,(uint8_t)(data->type),data->txData[0],data->txData[1],data->txData[2],data->txData[3],data->txData[4],data->txData[5],data->txData[6],data->txData[7],data->crc};

	HAL_UART_Transmit_IT(&huart1,data_frame_tx,DATA_SIZE);	//sending with interrupts
	data->crc=0;		//reset CRC after data transmission
}

void bluetooth_load_rx_data(bt_data *data)
{
	/*load data to data structure from data_frame_rx buffer*/

	for (uint8_t i=0; i<11; i++)
	{

		if(i<1)
		{
			data->header=data_frame_rx[i];	//writing received header into structure
		}

		if(i==1)
		{
			data_identyfier=data_frame_rx[i];	//copying identifier of message
		}

		if((i>1) && (i<10))
		{
			data->rxData[i-2]=data_frame_rx[i];  //copying data bytes
		}

		if(i>=10)	//computing CRC for received data
		{
			data_recognition(data,data_identyfier);	 //recognise type of message
			(data->crc)=((data->header)+(data->rxData[0])+(data->rxData[1])+(data->rxData[2])+(data->rxData[3])+(data->rxData[4])+(data->rxData[5])+(data->rxData[6])+(data->rxData[7])+(data->type)) % 256; //computing CRC
		}

	}//for


}//end of function

void bluetooth_IT_enabled(bt_data *data)
{
	/*enable callback from 7 bytes received and write data to data_frame_rx buffer*/

	HAL_UART_Receive_IT(&huart1,data_frame_rx, DATA_SIZE-1);	//enable hearing for received data (callback after 7 bytes received)
}


uint8_t* float2uint8(float f_value)
{

	/* function convert the float value to buffer of four uint8_t value
	 * you have to remember that MSB of float value  is in uint_value[3] and LSB is in uint_value[0] !!!
	 */

	to_convert my_union={0};
	to_convert* wsk=&my_union;
	wsk->value_float=f_value;

	for(uint8_t i=0;i<8;i++)
	{
		uint_value[i]=wsk->value_uint[i];
	}

	return uint_value;		//return pointer to uint8_t values buffer
}

uint8_t* float2_two_bytes(float f_value)
{

	/*FUNCTION FOR PARAMETERS WHICH CAN HAVE NEGATIVE VALUE, TOO!!!
	 *
	 *
	 * function convert float to 2 bytes integer
	 * */

	to_convert my_union={0};
	to_convert* wsk=&my_union;
	//wsk->value_float=f_value;
	int16_t a=(f_value)*100;
	wsk->value_int16=a;

	for(uint8_t i=0;i<8;i++)
	{
		uint_value[i]=wsk->value_uint[i];
	}

	return uint_value;		//return pointer to uint8_t values buffer


}

uint8_t* float2_two_bytes_unsigned(float f_value)
{

	/*FUNCTION ONLY FOR PARAMETERS WITH POSITIVE VALUE!!!
	 *
	 *
	 * function convert float to 2 bytes integer
	 * */

	to_convert my_union={0};
	to_convert* wsk=&my_union;
	//wsk->value_float=f_value;
	uint16_t a=(f_value)*100;
	wsk->value_uint16_t=a;

	for(uint8_t i=0;i<8;i++)
	{
		uint_value[i]=wsk->value_uint[i];
	}

	return uint_value;		//return pointer to uint8_t values buffer


}





uint8_t* double2_six_bytes(double d_value)
{
/*FUNCTION FOR CONVERTING GEOGRAPHICAL COORDINATES*/

	to_convert my_union={0};
	to_convert* wsk=&my_union;
	//wsk->value_double=d_value;
	long long int g=d_value*100000000000;
	wsk->value_long_int=g;

	for(uint8_t i=0;i<8;i++)
	{
		uint_value[i]=wsk->value_uint[i];
	}

	return uint_value;		//return pointer to uint8_t values buffer


}
