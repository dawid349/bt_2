/*
 * bluetooth.h
 *
 *  Created on: 12 mar 2020
 *      Author: Dawid
 */

#ifndef INC_BLUETOOTH_H_
#define INC_BLUETOOTH_H_

#include "stm32f4xx_hal.h"

#define DATA_SIZE 11				//length of frame in bytes
#define BT_DATA_TYPE data->type //macro for writing bt_data_type


typedef enum					//list of messages types
{
	VELOCITY,
	ENGINE_TEMP,
	LED_ON,
	LED_OFF,
	AIR_TEMP,
	ACCELERATION,
	BATTERY_LEVEL,
	DISPLAY_DATA,
	DRIVE_MODE,
	DEBUG_ST

} btDataType;

typedef struct						//type of bluetooth data frame
{
	uint8_t header;				//header
	btDataType type;				//data identyfier
	uint8_t txData[DATA_SIZE-3];	//data to send
	uint8_t rxData[DATA_SIZE-3];	//received data
	uint8_t crc;					//CRC

} bt_data;


typedef union
{
	float value_float;
	uint8_t value_uint[8];
	int16_t value_int16;
	uint16_t value_uint16_t;
	long long int value_long_int;
}to_convert;


void bluetooth_send_data(bt_data *data);
void bluetooth_load_rx_data(bt_data *data);
void bluetooth_IT_enabled(bt_data *data);
uint8_t* float2uint8(float f_value);
uint8_t* float2_two_bytes(float f_value);
uint8_t* double2_six_bytes(double d_value);
uint8_t* float2_two_bytes_unsigned(float f_value);
#endif /* INC_BLUETOOTH_H_ */
